<?php
/*
 * Copyright Information
 * @copyright: (c) 2022 agentur fipps e.K.
 * @author   : Arne Borchert <arne.borchert@fipps.de>
 * @license  : LGPL 3.0+
 */

// legends
$GLOBALS['TL_LANG']['tl_calendar_events']['microdata_legend'] = 'Microdata';

// fields
$GLOBALS['TL_LANG']['tl_calendar_events']['eventStatus']         = ['Status', ''];
$GLOBALS['TL_LANG']['tl_calendar_events']['eventAttendanceMode'] = ['Teilnahme Modus', ''];
$GLOBALS['TL_LANG']['tl_calendar_events']['organizer_name']      = ['Veranstalter Name', 'Name der Organisation'];
$GLOBALS['TL_LANG']['tl_calendar_events']['organizer_url']       = ['Veranstalter URL', 'URL der Organisation'];
$GLOBALS['TL_LANG']['tl_calendar_events']['performer']           = ['Veranstaltungsleiter', 'Name des Künstlers, Sprechers, Moderators, etc '];

// options
$GLOBALS['TL_LANG']['tl_calendar_events']['eventStatusOptions']         = [
    'EventScheduled'   => 'geplant',
    'EventRescheduled' => 'geändert',
    'EventMovedOnline' => 'geändert auf online',
    'EventPostponed'   => 'verschoben (bis auf weiteres)',
    'EventCancelled'   => 'abgesagt',
];
$GLOBALS['TL_LANG']['tl_calendar_events']['eventAttendanceModeOptions'] = [
    'MixedEventAttendanceMode'   => 'mixed',
    'OfflineEventAttendanceMode' => 'offline',
    'OnlineEventAttendanceMode'  => 'online',
];