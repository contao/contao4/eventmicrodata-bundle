<?php
/*
 * Copyright Information
 * @copyright: (c) 2022 agentur fipps e.K.
 * @author   : Arne Borchert <arne.borchert@fipps.de>
 * @license  : LGPL 3.0+
 */

// legends
$GLOBALS['TL_LANG']['tl_calendar_events']['microdata_legend'] = 'Microdata';

// fields
$GLOBALS['TL_LANG']['tl_calendar_events']['eventStatus']         = ['Status', ''];
$GLOBALS['TL_LANG']['tl_calendar_events']['eventAttendanceMode'] = ['Attendance mode', ''];
$GLOBALS['TL_LANG']['tl_calendar_events']['organizer_name']      = ['Organizer name', 'Name of the organization'];
$GLOBALS['TL_LANG']['tl_calendar_events']['organizer_url']       = ['Organizer URL', 'URL of the organization'];
$GLOBALS['TL_LANG']['tl_calendar_events']['performer']           = ['performer', 'name of the artist, speaker, etc'];

// options
$GLOBALS['TL_LANG']['tl_calendar_events']['eventStatusOptions']         = [
    'EventScheduled'   => 'scheduled',
    'EventRescheduled' => 'rescheduled',
    'EventMovedOnline' => 'moved online',
    'EventPostponed'   => 'postponed',
    'EventCancelled'   => 'cancelled',
];
$GLOBALS['TL_LANG']['tl_calendar_events']['eventAttendanceModeOptions'] = [
    'MixedEventAttendanceMode'   => 'mixed',
    'OfflineEventAttendanceMode' => 'offline',
    'OnlineEventAttendanceMode'  => 'online',
];