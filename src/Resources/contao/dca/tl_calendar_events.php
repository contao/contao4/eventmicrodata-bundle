<?php
/*
 *  Copyright Information
 *  
 *  @copyright: 2021 agentur fipps e.K.
 *  @author   : Arne Borchert
 *  @license  : LGPL 3.0+
 */

use Contao\CoreBundle\DataContainer\PaletteManipulator;

$table = 'tl_calendar_events';
$dca   = &$GLOBALS['TL_DCA'][$table];

PaletteManipulator::create()
                  ->addLegend('microdata_legend', 'date_legend', PaletteManipulator::POSITION_AFTER, true)
                  ->addField('eventStatus', 'microdata_legend', PaletteManipulator::POSITION_APPEND)
                  ->addField('eventAttendanceMode', 'microdata_legend', PaletteManipulator::POSITION_APPEND)
                  ->addField('organizer_name', 'microdata_legend', PaletteManipulator::POSITION_APPEND)
                  ->addField('organizer_url', 'microdata_legend', PaletteManipulator::POSITION_APPEND)
                  ->addField('performer', 'microdata_legend', PaletteManipulator::POSITION_APPEND)
                  ->applyToPalette('default', $table);

$dca['fields']['eventStatus']         = [
    'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events']['eventStatus'],
    'exclude'   => true,
    'inputType' => 'select',
    'options'   => &$GLOBALS['TL_LANG']['tl_calendar_events']['eventStatusOptions'],
    'eval'      => [
        'includeBlankOption' => true,
        'mandatory'          => false,
        'maxlength'          => 255,
        'tl_class'           => 'w50 clr',
    ],
    'sql'       => "varchar(255) NOT NULL default ''",
];
$dca['fields']['eventAttendanceMode'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events']['eventAttendanceMode'],
    'exclude'   => true,
    'inputType' => 'select',
    'options'   => &$GLOBALS['TL_LANG']['tl_calendar_events']['eventAttendanceModeOptions'],
    'eval'      => [
        'includeBlankOption' => true,
        'mandatory'          => false,
        'maxlength'          => 255,
        'tl_class'           => 'w50',
    ],
    'sql'       => "varchar(255) NOT NULL default ''",
];
$dca['fields']['organizer_name']      = [
    'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events']['organizer_name'],
    'exclude'   => true,
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'maxlength' => 255,
        'tl_class'  => 'w50 clr',
    ],
    'sql'       => "varchar(255) NOT NULL default ''",
];
$dca['fields']['organizer_url']       = [
    'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events']['organizer_url'],
    'exclude'   => true,
    'inputType' => 'text',
    'eval'      => [
        'mandatory'      => false,
        'rgxp'           => 'url',
        'decodeEntities' => true,
        'maxlength'      => 255,
        'dcaPicker'      => true,
        'addWizardClass' => false,
        'tl_class'       => 'w50',
    ],
    'sql'       => "varchar(255) NOT NULL default ''",
];
$dca['fields']['performer']           = [
    'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events']['performer'],
    'exclude'   => true,
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'maxlength' => 255,
        'tl_class'  => 'clr w50',
    ],
    'sql'       => "varchar(255) NOT NULL default ''",
];
